import React, { useState } from 'react'
import Head from 'next/head'
import { GetStaticProps, InferGetStaticPropsType } from 'next'
import useSWR from 'swr'

import Layout from 'components/Layout'
import AdminNav from 'components/AdminNav'
import DataDisplay from 'components/DataDisplay'

import { ArtworkResource, DealerGalleryView } from '@/lib/flow/models'
//import { listPackInfos } from '@/lib/data/data'
import { fetcher } from '@/lib/fetcher'
import { readAllArtwork } from '@/flow/scripts/read-all-artwork.script'
import { getNextGalleryId } from '@/flow/scripts/read-next-gallery-id.script'
import { readGalleryView } from '@/flow/scripts/read-gallery-view.script'
import { post } from '@/lib/fetch'
import { Message } from '@/lib/data/models'

type Prices = { lowPrice: number, highPrice: number }
type Mint = { setId: number, artId: number, quantity: number }

export const getStaticProps: GetStaticProps = async () => {
  const galleryId: number = await getNextGalleryId()

  return {
    props: {
      galleryCount: galleryId - 1
    }
  }
}

export default function Dealer({ galleryCount }: InferGetStaticPropsType<typeof getStaticProps>) {
  //const { data, error } = useSWR('/api/admin/packs/infos', fetcher)
  const [message, setMessage] = useState('')
  const [selected, setSelected] = useState(-1)
  const [artwork, setArtwork] = useState([])
  const [galleryView, setGalleryView] = useState({
    galleryId: 0,
    lowPrice: 0,
    highPrice: 0,
    isOpen: false,
    prices: [],
    collections: {}
  })


  // galleryCount of 1, means just id 1.
  // galleryCOunt of 4 maeans ids 1, 2, 3, 4.
  const galleryIds: number[] = [...Array(galleryCount).keys()].map(val => val+1)

  const onNewGallery = (event => {
    event.preventDefault()
    const body: Prices = {
      lowPrice: event.target.lowPrice.value,
      highPrice: event.target.highPrice.value
    }
    post<Prices, Message>(`/api/gallery/create`, body).then(res => {
      setMessage(res.message)
    })
  })

  const onSelectGallery = (num => {
    setSelected(num)
    readAllArtwork().then((res: ArtworkResource[]) => { setArtwork(res)})
    readGalleryView(num).then((res: DealerGalleryView) => { setGalleryView(res)})
  })

  const onOpenGallery = (() => {
    post<{isOpen: boolean}, Message>(`/api/gallery/${selected}/open`, {isOpen: true}).then(res => { })
  })
  const onCloseGallery = (() => {
    post<{isOpen: boolean}, Message>(`/api/gallery/${selected}/open`, {isOpen: true}).then(res => { })
  })

  const onMint = ((mint: Mint) => {
    post<Mint, Message>(`/api/gallery/${selected}/mint`, mint).then(res => { })
  })


  const renderGalleryView = () => (<>
    <h2>Gallery {galleryView.galleryId}</h2>
    {galleryView.isOpen && (<b>OPEN</b>)}
    {!galleryView.isOpen && (<b>CLOSED</b>)}
    <p>
      <button onClick={onOpenGallery}>Open</button>&nbsp;
      <button onClick={onCloseGallery}>Close</button>
    </p>
    <div className="grid">{galleryView.prices.map((price) => (<div>{price}</div>))}</div>    
  </>)



const renderGallery= () => (<>
  {artwork && (
    <div className="grid">
        {artwork.map((art) => (
          <div className="art" key={art.artId}>
            <object
              data={`data:image/svg+xml;base64,${btoa(art.data1 + art.data2)}`}
              type={art.mediaType}
              title={art.metadata.title}></object>
            <h2>{art.artId}: {art.metadata.title}</h2>
            <p><button onClick={()=>{onMint({setId: 1, artId: art.artId, quantity: 30})}}>Mint 30</button></p>
          </div>
        ))}
    </div>
    )}
</>)


  return <Layout>
    <Head>
      <title>Fresh Admin: Dealer</title>
    </Head>

    <h1>Admin</h1>
    <AdminNav current="dealer" />
    <h2>Dealer</h2>

    <hr/>
    <p>Gallery Count On Chain: { galleryIds.length }</p>
    <form onSubmit={onNewGallery}>
      <label htmlFor="lowPrice">Low Price:</label> <input id="lowPrice" type="text" required />
      <label htmlFor="highPrice">High Price:</label> <input id="highPrice" type="text" required />
      <button type="submit">New Gallery</button>
    </form>
    {message}
      <div className="links-list">
        {galleryIds.map((num) => (<span key={num}>
          Gallery {num}
          {selected !== num && (<button onClick={() => {onSelectGallery(num)}}>select</button>)}          
        </span>))}
      </div>
    <hr/>

    {(selected != -1) && renderGalleryView()}
    <hr/>
    {(selected != -1) && renderGallery()}

  </Layout>
}