import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import { GetStaticProps, InferGetStaticPropsType } from 'next'

import useSWR, { mutate } from 'swr'
import Layout from 'components/Layout'
import AdminNav from 'components/AdminNav'
import { Message } from '@/lib/data/models'
import { ArtworkResource } from '@/lib/flow/models'
import { readAllArtwork } from '@/flow/scripts/read-all-artwork.script'
import { readArtIdsInSet } from '@/flow/scripts/read-art-ids-in-set.script'

import { fetcher } from '@/lib/fetcher'
import { post, put } from '@/lib/fetch'

export const getStaticProps: GetStaticProps = async () => {
  const artwork: ArtworkResource[] = await readAllArtwork()
  const idsInSet: number[] | null = await readArtIdsInSet(1)

  return {
    props: {
      artwork: artwork,
      idsInSet: idsInSet
    }
  }
}

export default function Artwork({ artwork, idsInSet }: InferGetStaticPropsType<typeof getStaticProps>) {
  //const setId = null
  //const { data, error } = useSWR(`/api/set/${setId}/artwork`, fetcher)
  //let artwork = data as ArtworkResource[]

  const [message, setMessage] = useState('')

  const onAddMultipleArt = (event => {
    event.preventDefault()
    const setId: number = 1
    const artIds: number[] = (event.target.artIds.value as string).split(',').map(a => parseInt(a))
    post<number[], Message>(`/api/admin/artwork/add-to-set/${setId}`, artIds).then(res => {
      setMessage(res.message)
    })
  })

  return <Layout>
    <Head>
      <title>Fresh Admin: Artwork</title>
    </Head>

    <h1>Admin</h1>
    <AdminNav current="artwork" />

    <h2>Artwork On Blockchain</h2>
    <br/>
    <p>
      Art Ids in Set 1: {idsInSet ? idsInSet.join(', ') : 'None'}
      <br/>
      <form onSubmit={onAddMultipleArt}>
        <label htmlFor="artIds">IDs:</label>
        <input id="artIds" type="text" required />
        <button type="submit">Add Multiple Art</button>
      </form>
      {message}
    </p>
    <br/>

    {!artwork && (
      <div className="message">Loading...</div>
    )}

    {artwork && (
    <div className="grid">
        {artwork.map((art) => (
          <div className="art" key={art.artId}>
            <object
              data={`data:image/svg+xml;base64,${btoa(art.data1 + art.data2)}`}
              type={art.mediaType}
              title={art.metadata.title}></object>
            <h2>{art.artId}: {art.metadata.title}</h2>
            <p>{art.metadata.artist}</p>
          </div>
        ))}
    </div>
    )}

  </Layout>
}

/*

            <div className="art">
              <object
                data={`data:image/svg+xml;base64,${btoa(draft.data1 + draft.data2)}`}
                type={draft.mediaType}
                title={draft.metadata_title}></object>
            </div>
*/