import Head from 'next/head'
import Router, { useRouter } from 'next/router'
import useSWR, { mutate } from 'swr'

import { DraftArtData } from '@/lib/data/models'
import Layout from '@/components/Layout'
import AdminNav from '@/components/AdminNav'
import DataDisplay from '@/components/DataDisplay'



import { fetcher } from '@/lib/fetcher'
import { post, put } from '@/lib/fetch'

const defaultDraft: DraftArtData = {
  setId: 1,
  mediaType: 'image/svg+xml',
  dataType: 'svg+js',
  metadata_title: '',
  metadata_artist: '',
  metadata_set: '',
  metadata_description: '',
  data1: '',
  data2: '',    
  isMinted: false
}

export default function DraftArt() {
  const router = useRouter()
  const draftId = router.query.draft

  let draft: DraftArtData
  let isNew = (draftId == 'new')
  const draftPath = `/api/admin/artwork/drafts/${draftId}`

  if (isNew) {
    draft = undefined
  } else if (draftId != undefined) {
    const { data, error } = useSWR(draftPath, fetcher)
    draft = data as DraftArtData
  }

  const onSubmit = (values => {
    if (isNew) {
      post<DraftArtData, {id: string}>(`/api/admin/artwork/drafts`, values).then(res => {
        Router.replace(`/admin/artwork/${res.id}`)
      })
    } else {
      put<DraftArtData, {id: string}>(draftPath, values).then(res => {
        mutate(draftPath)
      })
    }
  })

  
  return  <Layout>
    <Head>
      <title>Fresh Admin: Artwork</title>
    </Head>

    <h1>Admin</h1>
    <AdminNav current="drafts" />

    <hr/>

    <DataDisplay
      model={draft}
      defaultData={defaultDraft}
      largeTextKeys={['data1', 'data2']}
      onSubmit={onSubmit} />

  </Layout>
}
