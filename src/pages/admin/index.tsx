import Head from 'next/head'
import { GetStaticProps, InferGetStaticPropsType } from 'next'
import useSWR, { mutate } from 'swr'

import Layout from 'components/Layout'
import AdminNav from 'components/AdminNav'
import DataDisplay from 'components/DataDisplay'
import DataEdit from 'components/DataEdit'
import { SetData } from '@/lib/data/models'
import { fetcher } from '@/lib/fetcher'
import { post } from '@/lib/fetch'

import { getNextSetId } from '@/flow/scripts/read-next-set-id.script'

export const getStaticProps: GetStaticProps = async () => {
  const setId: number = await getNextSetId()

  return {
    props: {
      setCount: setId - 1
    }
  }
}

const defaultSet: SetData = {
  setId: null,
  name: '',
  description: '',
  isLocked: false
}

export default function Admin({ setCount }: InferGetStaticPropsType<typeof getStaticProps>) {
  const { data, error } = useSWR('/api/sets', fetcher)
  let sets: SetData[] = data as SetData[]

  const onSubmit = (values => {
    post<SetData, {id: string}>(`/api/sets`, values).then(res => {
      mutate('/api/sets')
    })
  })

  return <Layout>
    <Head>
      <title>Fresh Admin: Sets</title>
    </Head>

    <h1>Admin</h1>
    <AdminNav current="sets" />
    <h2>Sets</h2>

    Set Count On Chain: { setCount }

    {error && (
      <div className="message">{ error }</div>
    )}

    {!sets && (
      <div className="message">Loading...</div>
    )}

    {sets &&
    sets.map((set) => (
      <div className="panel" key="">
        <h2>{set.setId}: {set.name}</h2>
        <h3>{set.description}</h3>
      </div>
    ))}
    
    <hr/>

    <h2>New</h2>

    <DataEdit
        model={null}
        defaultData={defaultSet}
        largeTextKeys={[]}
        onSubmit={onSubmit}
        onCancel={null} />

  </Layout>
}