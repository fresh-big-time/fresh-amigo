import React, { useState } from 'react'
import Head from 'next/head'
import Link from 'next/link'
import useSWR, { mutate } from 'swr'
import Layout from 'components/Layout'
import AdminNav from 'components/AdminNav'
import { DraftArtData } from '@/lib/data/models'
import { ArtworkResource } from '@/lib/flow/models'
import { readAllArtwork } from '@/flow/scripts/read-all-artwork.script'

import { fetcher } from '@/lib/fetcher'
import { post, put } from '@/lib/fetch'

export default function Artwork() {
  const [isLoading, setLoading] = useState(false)

  const { data: draftData, error: draftError } = useSWR(`/api/admin/artwork/drafts`, fetcher)
  let drafts = draftData as DraftArtData[]

  const onCreateArt = (draftArtId => {
    setLoading(true)
    const createPath = `/api/admin/artwork/create`
    post<{id: string}, {id: string}>(createPath, {id: draftArtId}).then(res => {
      setLoading(false)
      mutate(createPath)
    })
  })

  return <Layout>
    <Head>
      <title>Fresh Admin: Artwork Drafts</title>
    </Head>

    <h1>Admin</h1>
    <AdminNav current="drafts" />

    <h2>Artwork Drafts</h2>

    <Link href="/admin/artwork/new">
      <a className="button">New</a>
    </Link>

    {draftError && (
      <div className="message">{ draftError }</div>
    )}

    {!drafts || isLoading && (
      <div className="message">Loading...</div>
    )}

    {drafts && !isLoading && (
    <div className="grid">
        {drafts.map((draft, index) => (
          <div className="art" key={index}>
            <object
              data={`data:image/svg+xml;base64,${btoa(draft.data1 + draft.data2)}`}
              type={draft.mediaType}
              title={draft.metadata_title}></object>
            <h2><Link href={'/admin/drafts/' + draft._id}>{draft.metadata_title}</Link></h2>
            <p>{draft.metadata_artist}</p>
            {!draft.isMinted && (
              <button onClick={() => onCreateArt(draft._id) }>Create</button>
            )}
          </div>
        ))}
    </div>
    )}

  </Layout>
}

/*

            <div className="art">
              <object
                data={`data:image/svg+xml;base64,${btoa(draft.data1 + draft.data2)}`}
                type={draft.mediaType}
                title={draft.metadata_title}></object>
            </div>
*/