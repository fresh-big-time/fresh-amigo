import React, { useState } from 'react'
import { GetServerSideProps } from 'next'
import Head from 'next/head'
import Link from 'next/link'
import useSWR, { mutate } from 'swr'
//import ProfileCluster from "components/ProfileCluster"
import { useCurrentUser } from "state/current-user"

import Layout from 'components/Layout'
import GalleryArtRow from '@/components/GalleryArtRow'
import ArtThumbnail from 'components/ArtThumbnail'
import { useAppContext } from 'state/AppContext'
import { ArtworkResource, DealerGalleryView, GalleryRow } from '@/lib/flow/models'
import { readAllArtwork } from '@/flow/scripts/read-all-artwork.script'
import { readGalleryView } from '@/flow/scripts/read-gallery-view.script'
import { Long } from 'mongodb'

/*
        <ProfileCluster address={cu.addr} />
        <ProfileCluster address="0xba1132bc08f82fe2" />
        <ProfileCluster address="0xf117a8efa34ffd58" />
        */

/*
export const getServerSideProps: GetServerSideProps = async (context: any) => {
  const host = context.req.headers.host;
  return { props: { host } }
}
*/

export default function Home() {
  const { isProduction, isTest, isLocal } = useAppContext()
  if (isProduction) {
    return (
      <Layout>
        <Head>
          <title>FRESH Big Time</title>
        </Head>  
        <p>This is production environment. Not ready yet.</p>  
      </Layout>
    )
  }

  const { data: artwork, error: artError } = useSWR<ArtworkResource[], Error>('artwork', () => readAllArtwork())
  const { data: galleryView, error: galError } = useSWR<DealerGalleryView, Error>('galleryView', () => readGalleryView(1))

  //var rows: GalleryRow[] = []
  //var isChecked: boolean[] = []

  const [loadedState, setLoaded] = useState<boolean>(false)
  const [rowsState, setRowsState] = useState<GalleryRow[]>([])
  const [checkedState, setCheckedState] = useState<boolean[]>([])
  const [chancesState, setChancesState] = useState<number[]>([])

  const calculate = () => {
    const total = rowsState.reduce<number>((t, row, i) => checkedState[i] ? t + row.available : t, 0)
    const chances = rowsState.map<number>((row, i) => {
      if (!checkedState[i]) { return 0 }
      return (row.available / total) * 100.0
    })
    setChancesState(chances)
  }

  if (galleryView && artwork && !loadedState) {
    console.log(`data loaded, making gallery rows`)
    const eachMintSize: number = 30
    var initialChecked: boolean[] = Array(galleryView.serials.length).fill(false)
    setRowsState( Object.keys(galleryView.serials).map((key, index) => {
      const artId: number = Number(key)
      const available: number[] = Object.assign([], galleryView.serials[key])
      const art: ArtworkResource = artwork.find(a => a.artId === artId)
      var anyAvailable = false
      const serials: boolean[] = Array.from({length: eachMintSize}, (_, n) => {
        const i = n + 1
        const index = available.indexOf(n + 1, 0)
        if (index > -1) {
          available.splice(index, 1)
          anyAvailable = true
          return true
        }
        return false
      })

      initialChecked[index] = anyAvailable  
      return {
        art: art,
        serials: serials,
        available: galleryView.serials[key].length
      }
    }) )
    setLoaded(true)
    setCheckedState(initialChecked)
    calculate()
  }

  const handleOnChange = (index) => {
    setCheckedState(checkedState.map((item, i) =>
      i === index ? !item : item
    ))
    calculate()
  }
  

  const cu = useCurrentUser()
  return (
    <Layout>
      <Head>
        <title>FRESH Big Time</title>
      </Head>

      {artError && (<div className="message">{ artError.message }</div>)}
      {galError && (<div className="message">{ galError.message }</div>)}

      ${Number(galleryView?.lowPrice).toFixed(2)} to ${Number(galleryView?.highPrice).toFixed(2)}

      <div>
        {(galleryView && artwork) && rowsState.map((row, i) => (
            <GalleryArtRow key={i} row={row} name="gallery" checked={checkedState[i]} chances={chancesState[i]} onChange={()=>{ handleOnChange(i) }} />
          )
        )}
      </div>

      
      <p>isProduction: {isProduction ? 'true' : 'false' }</p>
      <p>isTest: {isTest ? 'true' : 'false' }</p>
      <p>isLocal: {isLocal ? 'true' : 'false' }</p>

    </Layout>
  )
}
