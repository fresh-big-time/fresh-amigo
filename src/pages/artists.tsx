import Head from 'next/head'
import Link from 'next/link'
import Layout from 'components/Layout'
import { useAppContext } from 'state/AppContext'

export default function Artists() {
  const { isProduction, isTest, isLocal } = useAppContext()

  return <Layout>
    <Head>
      <title>Artists</title>
    </Head>

    <h1>Artists</h1>

    <Link href="/">Index</Link>
      
      <p>isProduction: {isProduction ? 'true' : 'false' }</p>
      <p>isTest: {isTest ? 'true' : 'false' }</p>
      <p>isLocal: {isLocal ? 'true' : 'false' }</p>

    <ul>
      <li><Link href="/artist/aurumino">Aurumino</Link></li>
      <li><Link href="/artist/protogenes">Protogenes</Link></li>
      <li><Link href="/artist/minerva">Minerva</Link></li>
      <li><Link href="/artist/wesley">Wesley</Link></li>
    </ul>

  </Layout>
}