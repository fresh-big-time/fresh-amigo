import type { NextApiRequest, NextApiResponse } from 'next'
import { Message } from '@/lib/data/models'
import { ArtworkResource } from '@/lib/flow/models'
import { readAllArtwork } from '@/flow/scripts/read-all-artwork.script'

export default async (req: NextApiRequest, res: NextApiResponse<ArtworkResource[] | Message>) => {
  const setIdString: string = req.query.set as string
  var setId: number | null = (setIdString === 'null') ? null : parseInt(setIdString)

  switch (req.method) {
    case 'GET':
      //return res.status(200).json(await listArt(setId))
      return res.status(200).json(await readAllArtwork())

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}