import type { NextApiRequest, NextApiResponse } from 'next'
import { EditionData, Message } from '@/lib/data/models'
import { listEditions } from '@/lib/data/data'

export default async (req: NextApiRequest, res: NextApiResponse<EditionData[] | Message>) => {
  const setId: number = parseInt(req.query.set as string)
  const artId: number = parseInt(req.query.art as string)

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listEditions(setId, artId))

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}