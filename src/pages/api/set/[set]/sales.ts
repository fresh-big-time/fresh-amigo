import type { NextApiRequest, NextApiResponse } from 'next'
import { SaleData } from '@/lib/data/models'
import { listSales } from '@/lib/data/data'

// Sales for just this set

export default async (req: NextApiRequest, res: NextApiResponse<SaleData[]>) => {
  const setId: number = parseInt(req.query.set as string)

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listSales(setId))
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }
}