import type { NextApiRequest, NextApiResponse } from 'next'
import { SaleData, Message } from '@/lib/data/models'
import { listSales, addSale, removeSale } from '@/lib/data/data'

export default (req: NextApiRequest, res: NextApiResponse<SaleData[] | Message>) => {

  switch (req.method) {
    case 'GET':     return get()
    case 'POST':     return post()
    case 'DELETE':  return del()
    default:
        return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function get() {
    const setId: number = parseInt(req.query.set as string)
    const artId: number = parseInt(req.query.art as string)
    return res.status(200).json(await listSales(setId, artId))
  }

  async function post() {
    const sale = req.body as SaleData
    try {
      // TRANSACTION for creating sale ask

      // Record sale in db
      const success = await addSale(sale)
      return res.status(200).json({message: success ? 'Added new sale.' : 'Failed to add sale.'})
    } catch (error) {
      return res.status(400).json({message: error.toString()})
    }
  }

  async function del() {
    const nftId: number = parseInt(req.query.nft as string)
    try {
      // TRANSACTION for removing nft id from sale

      // Remove sale in db
      const count = await removeSale(nftId)
      return res.status(200).json({message: `Deleted ${count} sale.`})
    } catch (error) {
      return res.status(400).json({message: error.toString()})
    }
  }

}