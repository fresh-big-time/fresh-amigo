import type { NextApiRequest, NextApiResponse } from 'next'
import { SaleData, Message } from '@/lib/data/models'
import { removeSale } from '@/lib/data/data'

export default async (req: NextApiRequest, res: NextApiResponse<Message>) => {
  if (req.method !== 'POST') { 
    return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  const nftId: number = parseInt(req.query.nft as string)

  try {

    // TRANSACTION for buying

    // Remove sale in db
    const count = await removeSale(nftId)
    return res.status(200).json({message: `Purchased ${count} item.`})
  } catch (error) {
    return res.status(400).json({message: error.toString()})
  }

}