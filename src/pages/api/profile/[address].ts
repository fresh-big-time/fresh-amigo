import type { NextApiRequest, NextApiResponse } from 'next'
import { fetchProfile } from "flow/fetch-profile.script"

type ProfileData = {
  status: string;
  isCurrentUser: boolean;
  setName: (name: any) => Promise<void>;
  refetch: () => Promise<void>;
  IDLE: string;
  PROCESSING: string;
  isIdle: boolean;
  isProcessing: boolean;

  name: string;
  color: string;
  info: string;
  avatar: string;
}

type ErrorData = {
  error: string,
  address: string | string[]
}

export default async (req: NextApiRequest, res: NextApiResponse<ProfileData | ErrorData>) => {
  const { address } = req.query
  const profile = await fetchProfile(address)
  //.catch(e => {
  //  res.status(204).json({'error': e, 'address': address})
  //})

  res.status(200).json(profile)
}