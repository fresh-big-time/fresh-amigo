import type { NextApiRequest, NextApiResponse } from 'next'
import { ArtData, Message } from '@/lib/data/models'
import { listArt, findOneDraftArtwork, addArt, draftArtworkSetIsMinted } from '@/lib/data/data'
import { createArt } from '@/flow/admin/create-art.tx'

export default async (req: NextApiRequest, res: NextApiResponse<ArtData[] | Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  const setId: number = parseInt(req.query.set as string)

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listArt(setId))

    case 'POST':
      const { id } = req.body as { id: string }
      const draftArt = await findOneDraftArtwork(id)
      
      if (draftArt == null) { return res.status(200).json({message: 'Failed to find Draft Artwork.'}) }

      // create art data object from Draft Art
      const art: ArtData = {
        artId: 0,
        setId: null, // not initally added to a set
        mediaType: draftArt.mediaType,
        dataType: draftArt.dataType,
        numberMinted: 0,
        retired: false,      
        metadata_title: draftArt.metadata_title,
        metadata_artist: draftArt.metadata_artist,
        metadata_set: draftArt.metadata_set,
        metadata_description: draftArt.metadata_description,        
        data1: draftArt.data1,
        data2: draftArt.data2
      }

      const txId = createArt(art)

      const createSuccess = await addArt(art)
      const updateSuccess = await draftArtworkSetIsMinted(draftArt)

      return res.status(200).json({message: createSuccess ? 'Added new art.' : 'Failed to add art.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}