import type { NextApiRequest, NextApiResponse } from 'next'
import { DraftArtData, Message } from '@/lib/data/models'
import { listDraftArtwork, addDraftArt } from '@/lib/data/data'

export default async (req: NextApiRequest, res: NextApiResponse<DraftArtData[] | { id: string }>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  const setId: number = parseInt(req.query.set as string)

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listDraftArtwork(setId))

    case 'POST':
      const draft = req.body as DraftArtData

      const id = await addDraftArt(draft)
      return res.status(200).json({ id: id })

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}