import type { NextApiRequest, NextApiResponse } from 'next'
import { Message } from '@/lib/data/models'
import { readAllArtwork } from '@/flow/scripts/read-all-artwork.script'
import { addMultipleArtToSet } from '@/flow/admin/add-multiple-to-set.tx'

export default async (req: NextApiRequest, res: NextApiResponse<Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  var setId: number = parseInt(req.query.set as string)

  switch (req.method) {
    case 'POST':
      const artIds = req.body as number[]
      const success = addMultipleArtToSet(setId, artIds)
      return res.status(200).json({message: success ? 'Success! Art ids added to set.' : 'Failed to add art ids to set.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}