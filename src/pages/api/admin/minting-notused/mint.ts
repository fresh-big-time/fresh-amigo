import type { NextApiRequest, NextApiResponse } from 'next'
import { EditionData, Message } from '@/lib/data/models'
import { listEditions, addEditions } from '@/lib/data/data'

export default async (req: NextApiRequest, res: NextApiResponse<EditionData[] | Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }

  const setId: number = parseInt(req.query.set as string)
  const artId: number = parseInt(req.query.art as string)

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listEditions(setId, artId))

    case 'POST':
      return res.status(200).json({message: 'Not implimented yet'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}