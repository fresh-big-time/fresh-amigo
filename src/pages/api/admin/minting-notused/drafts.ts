import type { NextApiRequest, NextApiResponse } from 'next'
import { DraftMintData, Message } from '@/lib/data/models'
import { listDraftMints, addDraftMint } from '@/lib/data/data'

export default async (req: NextApiRequest, res: NextApiResponse<DraftMintData[] | Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listDraftMints())

    case 'POST':
      const { setId, artId, quantity } = req.body
      const success = await addDraftMint(setId, artId, quantity)
      return res.status(200).json({message: success ? 'Added Draft.' : 'Failed to add Draft.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}