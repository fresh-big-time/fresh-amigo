import type { NextApiRequest, NextApiResponse } from 'next'
import { Message } from '@/lib/data/models'
import { fulfill } from '@/flow/admin/dealer-fulfill.tx'

export default async (req: NextApiRequest, res: NextApiResponse<Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  const galleryId: number = parseInt(req.query.gallery as string)
  
  switch (req.method) {
    case 'GET':
      const txId = await fulfill(galleryId)
      return res.status(200).json({message: txId ? 'FULFILLED' : 'Failed to fulfill, maybe none needed.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}