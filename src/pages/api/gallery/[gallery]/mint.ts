import type { NextApiRequest, NextApiResponse } from 'next'
import { Message } from '@/lib/data/models'
import { mintCollectibles } from '@/flow/admin/dealer-mint-collectibles.tx'

export default async (req: NextApiRequest, res: NextApiResponse<Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  const galleryId: number = parseInt(req.query.gallery as string)
  
  switch (req.method) {
    case 'POST':
      const { setId, artId, quantity } = req.body as {
        setId: number, artId: number, quantity: number
      }
      const txId = await mintCollectibles(setId, artId, quantity, galleryId)
      return res.status(200).json({message: txId ? 'SUCCESS, minted and added to gallery.' : 'Failed to mint and add to gallery.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}