import type { NextApiRequest, NextApiResponse } from 'next'
import { Message } from '@/lib/data/models'
import { openGallery } from '@/flow/admin/dealer-open-gallery.tx'

export default async (req: NextApiRequest, res: NextApiResponse<Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  const galleryId: number = parseInt(req.query.gallery as string)
  
  switch (req.method) {
    case 'POST':
      const { isOpen } = req.body as { isOpen: boolean }
      const txId = await openGallery(galleryId, isOpen)
      return res.status(200).json({message: txId ? 'SUCCESS, changed open status.' : 'Failed to update open.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}