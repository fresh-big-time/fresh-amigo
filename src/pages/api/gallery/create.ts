import type { NextApiRequest, NextApiResponse } from 'next'
import { Message } from '@/lib/data/models'
import { createGallery } from '@/flow/admin/dealer-create-gallery.tx'

export default async (req: NextApiRequest, res: NextApiResponse<Message>) => {
  if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
  
  switch (req.method) {
    case 'POST':
      const { lowPrice, highPrice } = req.body as { lowPrice: number, highPrice: number }

      const txId = await createGallery(lowPrice, highPrice)
      return res.status(200).json({message: txId ? 'SUCCESS Created new gallery.' : 'Failed to create gallery.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}