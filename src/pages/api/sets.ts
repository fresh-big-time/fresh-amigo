import type { NextApiRequest, NextApiResponse } from 'next'
import { SetData, Message } from '@/lib/data/models'
import { listSets, addSet } from '@/lib/data/data'

import { createSet } from '@/flow/admin/create-set.tx'
import { getNextSetId } from '@/flow/scripts/read-next-set-id.script'

import * as fcl from "@onflow/fcl"

export default async (req: NextApiRequest, res: NextApiResponse<SetData[] | Message>) => {

  switch (req.method) {
    case 'GET':
      return res.status(200).json(await listSets())

    case 'POST':
      if (!process.env.MONGODB_ISADMIN) { return res.status(403).end('Forbidden') }
      var set = req.body as SetData

      const nextSetId = await getNextSetId()
      //console.log(`nextSetId: ${nextSetId}`)

      // TRANSACTION for new Set Data
      await createSet(set.name, set.description)

      //success?
      set.setId = nextSetId

      const success = await addSet(set)
      return res.status(200).json({message: success ? 'Added new set.' : 'Failed to add set.'})

    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

}