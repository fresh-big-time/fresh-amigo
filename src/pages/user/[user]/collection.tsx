import { useRouter } from 'next/router'
import Layout from 'components/Layout'

export default function UserCollection() {
  const router = useRouter()
  const { user } = router.query

  return <Layout>
    <h1>{user}</h1>
  </Layout>
}