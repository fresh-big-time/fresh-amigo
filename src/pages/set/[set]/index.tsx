import { useRouter } from 'next/router'
import Head from 'next/head'
import Link from 'next/link'

import Layout from 'components/Layout'
import ArtThumbnail from 'components/ArtThumbnail'

export default function Artist() {
  const router = useRouter()
  const { set } = router.query

  return <Layout>
    <Head>
      <title>Set: {set}</title>
    </Head>

    <h1>{set}</h1>

    <div className="grid">
      <ArtThumbnail />
      <ArtThumbnail />
      <ArtThumbnail />
      <ArtThumbnail />
      <ArtThumbnail />
      <ArtThumbnail />
      <ArtThumbnail />
      <ArtThumbnail />
    </div>

  </Layout>
}