import { useRouter } from 'next/router'
import Head from 'next/head'
import Link from 'next/link'

import Nav from 'components/Nav'
import ArtDetail from 'components/ArtDetail'

export default function ArtistArt() {
  const router = useRouter()
  const { set, art } = router.query

  return <>
    <Nav/>
    <div className="display">

      <input type="checkbox" id="zoom" role="button" />
      <aside>
        <div className="nav-height"></div>
        <div className="art">
          <label htmlFor="zoom">
            <img src="/art/art_005.svg" alt="Image Title" />
          </label>
          <label className="zoom-toggle" htmlFor="zoom"></label>
        </div>
      </aside>


      <main>
        <div className="nav-height hide-small"></div>

        <h1>Title High</h1>

        <div>
          <h2>Properties</h2>
          <div className="detail">
            <label>Artist</label>
            <div>
              <a className="artist" href="art.html">
                <b>Mondrian 5</b>
                <span data-value="23.5" data-change="+1.4">23.5 +1.4%</span>
              </a>
            </div>
          </div>
          <div className="detail">
            <label>Series</label>
            <div>Future Shock</div>
          </div>
          <div className="detail">
            <label>Editions</label>
            <div>10001</div>
          </div>
          <div className="detail">
            <label>Medium</label>
            <div>Vector</div>
          </div>
          <div className="detail">
            <label>Dimensions</label>
            <div>123 px by 123 px</div>
          </div>
          <div className="detail">
            <label>Owned by</label>
            <div>Username</div>
          </div>
        </div>


        <div>
          <h2>Availability</h2>
          <div className="detail">
            <label>Ask</label>
            <div>$45.50</div>
            <a className="button" href="art.html">Buy</a>
          </div>
        </div>

        <div>
          <h2>Purchase History</h2>
          <div className="detail">
            <label>January 12, 2021</label>
            <div>Username purchased for $34.00</div>
          </div>
          <div className="detail">
            <label>January 12, 2021</label>
            <div>Username purchased for $34.00</div>
          </div>
          <div className="detail">
            <label>January 12, 2021</label>
            <div>Username purchased for $34.00</div>
          </div>
        </div>
      </main>


    </div>
  </>
}