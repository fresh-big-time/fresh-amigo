import { useRouter } from 'next/router'
import Head from 'next/head'
import Link from 'next/link'

import Layout from 'components/Layout'
import ArtDetail from 'components/ArtDetail'

export default function ArtistArt() {
  const router = useRouter()
  const { set, art, edition } = router.query

  return <Layout>
    <Head>
      <title>Art by {set}</title>
    </Head>

    <h1>{set}</h1>
    <h2>{art}</h2>
    <h2>{edition}</h2>

    <ArtDetail title="Title" edition={1001} />
  </Layout>
}