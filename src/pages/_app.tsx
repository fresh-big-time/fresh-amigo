import App from "next/app";
import type { AppProps, AppContext } from 'next/app'
import { RecoilRoot } from 'recoil'

import { CurrentUserSubscription } from "state/current-user"
import { AppProvider, AppContextType } from "state/AppContext"


import 'styles/global.scss'

function MyApp({ Component, pageProps }: AppProps) {
  //console.log(`MyApp --  isProduction: ${pageProps.contextValue?.isProduction}`)
  //globals = pageProps.contextValue;
  
  return <RecoilRoot>
    <AppProvider>
      <CurrentUserSubscription />
      <Component {...pageProps} />
    </AppProvider>
  </RecoilRoot>
}

// This disables the ability to static optimization, causing every page in your app to
// be server-side rendered.
/*
MyApp.getInitialProps = async (appContext: AppContext) => {
  // calls page's `getInitialProps` and fills `appProps.pageProps`
  const appProps = await App.getInitialProps(appContext);
  const host = appContext.ctx?.req?.headers.host;

  console.log(`getInitialProps  host: ${host}`)

  // pass initial AppContext values to pageprops, then passed above directly to context.
  if (host !== undefined) {
    console.log(`HOST_IS_PRODUCTION ${process.env.HOST_IS_PRODUCTION}`)
    console.log(`MONGODB_ISADMIN ${process.env.MONGODB_ISADMIN}`)
    appProps.pageProps.contextValue = {
      isProduction: host === process.env.HOST_IS_PRODUCTION,
      isTest: host === process.env.HOST_IS_TEST,
      isLocal: host === process.env.HOST_IS_TEST,
      isAdmin: process.env.MONGODB_ISADMIN === "true"
    }
  }

  return { ...appProps }
}
*/

// Only uncomment this method if you have blocking data requirements for
// every single page in your application. This disables the ability to
// perform automatic static optimization, causing every page in your app to
// be server-side rendered.
//
// MyApp.getInitialProps = async (appContext: AppContext) => {
//   // calls page's `getInitialProps` and fills `appProps.pageProps`
//   const appProps = await App.getInitialProps(appContext);

//   return { ...appProps }
// }

export default MyApp