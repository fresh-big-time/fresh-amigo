import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"

// Allows Dealer to redeem a random NFT to somebody wihtout having to pay
export async function redeem(galleryId: number, artIds: number[], recipientAddress: string): Promise<string> {

  const code = `import FreshDealer from 0xFreshDealer  
  transaction(galleryId: UInt32, artIds: [UInt32], recipientAddress: Address) {
    // The token being transfered
    let token: @NonFungibleToken.NFT

    prepare(acct: AuthAccount) {
        let dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")

        self.token <- dealerRef.randomWithdraw(galleryId: galleryId, artIds: artIds)
    }

    execute {
        // Get the recipient's public account object
        let recipient = getAccount(recipientAddress)

        // Borrow the Collection reference for the receiver
        let receiverRef = recipient.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.CollectionPublic}>()
                ?? panic("Cannot borrow a reference to the recipient's collection")

        // Deposit the NFT in the receivers collection
        receiverRef.deposit(token: <-self.token)
    }
  }`
  
  const args: any[] = [
    fcl.arg(galleryId, t.UInt32),
    fcl.arg(artIds, t.Array(t.UInt32)),
    fcl.arg(recipientAddress, t.Address)
  ]

  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}