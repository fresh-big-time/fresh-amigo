import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"

// Allows Dealer owner to open or close a gallery
// Parameters:
// galleryId: Dealer gallery id
// isOpen: true to open, false to close
export async function openGallery(galleryId: number, isOpen: boolean): Promise<string> {

  const code = `import FreshDealer from 0xFreshDealer  
  transaction(galleryId: UInt32, isOpen: Bool) {
    let dealerRef: &FreshDealer.Dealer

    prepare(acct: AuthAccount) {
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
    }

    execute {
        self.dealerRef.setOpenForSale(galleryId: galleryId, isOpen: isOpen)
    }
  }`
  
  const args: any[] = [
    fcl.arg(galleryId, t.UInt32),
    fcl.arg(isOpen, t.Bool)
  ]

  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}