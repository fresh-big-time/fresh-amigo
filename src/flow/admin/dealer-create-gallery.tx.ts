import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"

// Allows Dealer owner to create a gallery. Gallery is empty and closed.
// Parameters:
// galleryId: Dealer gallery id
// TODO: Switch FT to particular, USD??
export async function createGallery(lowPrice: number, highPrice: number): Promise<string> {

  const code = `import FreshDealer from 0xFreshDealer
  import FungibleToken from 0xFungibleToken
  transaction(lowPrice: UFix64, highPrice: UFix64) {
    let dealerRef: &FreshDealer.Dealer
    let paymentReceiver: Capability<&{FungibleToken.Receiver}>
    let currentGalleryId: UInt32

    prepare(acct: AuthAccount) {
        self.currentGalleryId = FreshDealer.nextGalleryId;
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
        self.paymentReceiver = acct.getCapability<&{FungibleToken.Receiver}>(/public/flowTokenReceiver)
    }

    execute {
        self.dealerRef.createGallery(
            paymentReceiver: self.paymentReceiver,
            lowPrice: lowPrice,
            highPrice: highPrice
        )
    }

    post {        
        self.dealerRef.borrowGallery(galleryId: self.currentGalleryId) != nil: "galleryId doesn't exist"
    }
  }`

  // .toFixed(2)} not working for some reason. TODO
  const args: any[] = [
    fcl.arg(`${lowPrice}.00`, t.UFix64),
    fcl.arg(`${highPrice}.00`, t.UFix64)
  ]

  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}