import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"

// Allows Dealer owner to fulfill any Purchases in Queue
// If fulfillment fails, the process will try to resolve with reimbursement
export async function fulfill(galleryId: number): Promise<string> {

  const code = `import FreshDealer from 0xFreshDealer  
  transaction(galleryId: UInt32) {
    let dealerRef: &FreshDealer.Dealer

    prepare(acct: AuthAccount) {
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
    }

    execute {
        self.dealerRef.fulfillQueue()
    }
  }`
  
  const args: any[] = [
    fcl.arg(galleryId, t.UInt32)
  ]

  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}