import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"

export async function createSet(name: string, description: string): Promise<string> {

  const code = `
  import FreshArtNFT from 0xFreshArtNFT
  
  transaction(setName: String, setDescription: String) {
      let adminRef: &FreshArtNFT.Admin
      let currentSetID: UInt32
  
      prepare(acct: AuthAccount) {
          // borrow a reference to the Admin resource in storage
          self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
              ?? panic("Could not borrow a reference to the Admin resource")
          self.currentSetID = FreshArtNFT.nextSetId
      }
  
      execute {        
          // Create a set with the specified name
          self.adminRef.createSet(name: setName, description: setDescription)
      }
  
      post {
          FreshArtNFT.getSetName(setId: self.currentSetID) == setName: "Could not find the specified set"
      }
  }
  `.trim()
  
  const { id, error, events } = await mutateWithAdmin(code, [ fcl.arg(name, t.String), fcl.arg(description, t.String) ])
  return id
}


//console.log(`tx[${txId}]: https://flow-view-source.com/testnet/tx/${txId}`) 