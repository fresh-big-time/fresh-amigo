import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"

// Account must be a admin for both, FreshArtNFT admin and FreshDealer owner

// Parameters:
//
// setId: the ID of a set containing the target play
// artId: the ID of a art to mint from
// quantity: the number of collectibles to be minted
// galleryId: Dealer gallery id to deposit NFTs in
export async function mintCollectibles(setId: number, artId: number, quantity: number, galleryId: number): Promise<string> {

  const code = `import FreshArtNFT from 0xFreshArtNFT
  import FreshDealer from 0xFreshDealer  
  transaction(setId: UInt32, artId: UInt32, quantity: UInt64, galleryId: UInt32) {
    let adminRef: &FreshArtNFT.Admin
    let dealerRef: &FreshDealer.Dealer

    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
        self.dealerRef = acct.borrow<&FreshDealer.Dealer>(from: FreshDealer.DealerStoragePath)
            ?? panic("Could not borrow a reference to the Dealer resource")
    }

    execute {
        // Borrow a reference to the specified set
        let setRef = self.adminRef.borrowSet(setId: setId)

        // Mint multiple tokens
        let collection <- setRef.batchMintCollectible(artId: artId, quantity: quantity)

        // Put tokens collection in the gallery
        self.dealerRef.addArtwork(galleryId: galleryId, artId: artId, collection: <- collection)
    }
  }`
  
  const args: any[] = [
    fcl.arg(setId, t.UInt32),
    fcl.arg(artId, t.UInt32),
    fcl.arg(quantity, t.UInt64),
    fcl.arg(galleryId, t.UInt32)
  ]

  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}