import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"
import type { ArtData } from "@/lib/data/models"

export async function addMultipleArtToSet(setId: number, artIds: number[]): Promise<string> {

  const code = `import FreshArtNFT from 0xFreshArtNFT  
  transaction(setId: UInt32, artIds: [UInt32]) {
    let adminRef: &FreshArtNFT.Admin
    
    prepare(acct: AuthAccount) {
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath)
            ?? panic("Could not borrow a reference to the Admin resource")
    }
    execute {
        let setRef = self.adminRef.borrowSet(setId: setId)
        setRef.addMultipleArt(artIds: artIds)
    }
  }`

  const args: any[] = [
    fcl.arg(setId, t.UInt32),
    fcl.arg(artIds, t.Array(t.UInt32))
  ]
  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}