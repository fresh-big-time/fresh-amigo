import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { mutateWithAdmin } from "@/lib/flow/query"
import type { ArtData } from "@/lib/data/models"

export async function createArt(art: ArtData): Promise<string> {

  const code = `
  import FungibleToken from 0xFungibleToken
  import FreshArtNFT from 0xFreshArtNFT
  
  transaction(artistAddress: Address, mediaType: String?, dataType: String?, data1: String?, data2: String?, metadata: {String: String}) {
    let adminRef: &FreshArtNFT.Admin
    let artistWallet: Capability<&{FungibleToken.Receiver}>
    let currentArtId: UInt32

    prepare(acct: AuthAccount) {
        self.currentArtId = FreshArtNFT.nextArtId;
        self.adminRef = acct.borrow<&FreshArtNFT.Admin>(from: FreshArtNFT.AdminStoragePath) ?? panic("No admin resource in storage")
        let artistAccount = getAccount(artistAddress)
        self.artistWallet = artistAccount.getCapability<&{FungibleToken.Receiver}>(/public/flowTokenReceiver)
    }

    execute {
        let royalties = {
            "artist": FreshArtNFT.Royalty(wallet: self.artistWallet, cut: 0.025)
        }
        self.adminRef.createArt(
            mediaType: mediaType,
            dataType: dataType,
            data1: data1,
            data2: data2,
            royalties: royalties,
            metadata: metadata
        )
    }

    post {        
        FreshArtNFT.getArtwork(artId: self.currentArtId) != nil: "artId doesn't exist"
    }
  }`.trim()

  // TODO Prod
  const artistAddress = process.env.TEST_FLOW_ACCOUNT_ADDRESS;

  const metadata: {key: string, value: string}[] = [
    {key: 'title', value: art.metadata_title},
    {key: 'artist', value: art.metadata_artist},
    {key: 'set', value: art.metadata_set},
    {key: 'description', value: art.metadata_description},
  ]
  
  const args: any[] = [
    fcl.arg(artistAddress, t.Address),
    fcl.arg(art.mediaType, t.Optional(t.String)),
    fcl.arg(art.dataType, t.Optional(t.String)),
    fcl.arg(art.data1, t.Optional(t.String)),
    fcl.arg(art.data2, t.Optional(t.String)),
    fcl.arg(metadata, t.Dictionary({key: t.String, value: t.String }))
  ]
  const { id, error, events } = await mutateWithAdmin(code, args)

  return id
}


//console.log(`tx[${txId}]: https://flow-view-source.com/testnet/tx/${txId}`) 

// Athena:    9f8d9a5b9d644067e039db5936269227c0e4e69959b161896dd58077e5eb6f05
// Classical: eaff608ae2ea0e27b1b9c699a4e37ea21374da1f06c92c8839e65457f2be2c5c