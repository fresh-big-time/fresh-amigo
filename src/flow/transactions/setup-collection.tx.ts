import * as fcl from "@onflow/fcl"
import { mutateWithWallet } from "@/lib/flow/query"

// This transaction configures a user's account
// to use the NFT contract by creating a new empty collection,
// storing it in their account storage, and publishing a capability
export async function setupCollection() {
  const code = `import FreshArtNFT from 0xFreshArtNFT
  transaction {
    prepare(account: AuthAccount) {
        // Create a new empty collection
        let collection <- FreshArtNFT.createEmptyCollection()
  
        // store the empty NFT Collection in account storage
        account.save<@FreshArtNFT.Collection>(<-collection, to: FreshArtNFT.CollectionStoragePath)
  
        // create a public capability for the Collection
        account.link<&{FreshArtNFT.CollectionPublic}>(
          FreshArtNFT.CollectionPublicPath,
          target: FreshArtNFT.CollectionStoragePath
        )
    }
  }`

  console.log("TRANSACTION: Creating collection")
  const { id, error, events } = await mutateWithWallet(code)
  console.log(`TX id: ${id}`)
  console.log(`TX error: ${error}`)
  return id
}