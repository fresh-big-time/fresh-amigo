import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"

// https://flow-view-source.com/testnet/account/0xd5ee212b0fa4a319/contract/Profile 

export async function fetchProfile(address) {
  if (address == null) return null

  return fcl
    .send([
      fcl.script`
        import Profile from 0xProfile

        pub fun main(address: Address): Profile.ReadOnly? {
          return Profile.read(address)
        }
      `,
      fcl.args([fcl.arg(address, t.Address)]),
    ])
    .then(fcl.decode)
}