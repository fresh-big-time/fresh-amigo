import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"

// This transaction transfers an NFT from one user's collection
// to another user's collection.

export async function transferArt() {
  const txId = await fcl
    .send([
      fcl.proposer(fcl.authz),
      fcl.payer(fcl.authz),
      fcl.authorizations([fcl.authz]),
      fcl.limit(35),
      fcl.args([fcl.arg(recipient, t.Address)]),
      fcl.transaction`
        import FreshArtNFT from 0xFreshArtNFT

        transaction {

          // The field that will hold the NFT as it is being
          // transferred to the other account
          let transferToken: @FreshArtNFT.NFT
        
          prepare(acct: AuthAccount) {
      
              // Borrow a reference from the stored collection
              let collectionRef = acct.borrow<&FreshArtNFT.Collection>(from: /storage/NFTCollection)
                  ?? panic("Could not borrow a reference to the owner's collection")
      
              // Call the withdraw function on the sender's Collection
              // to move the NFT out of the collection
              self.transferToken <- collectionRef.withdraw(withdrawID: 1)
          }
      
          execute {
              // Get the recipient's public account object
              let recipient = getAccount(0x01)
      
              // Get the Collection reference for the receiver
              // getting the public capability and borrowing a reference from it
              let receiverRef = recipient.getCapability<&{FreshArtNFT.NFTReceiver}>(/public/NFTReceiver)
                  .borrow()
                  ?? panic("Could not borrow receiver reference")
      
              // Deposit the NFT in the receivers collection
              receiverRef.deposit(token: <-self.transferToken)
      
              log("NFT ID 1 transferred from account 2 to account 1")
          }
        }
       
      `,
    ])
    .then(fcl.decode)

  return fcl.tx(txId).onceSealed()
}