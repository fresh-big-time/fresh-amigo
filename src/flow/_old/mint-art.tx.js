import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"

// This transaction allows the Minter account to mint an NFT
// and deposit it into its collection.

export async function mintArtNFT() {
  const txId = await fcl
    .send([
      fcl.proposer(fcl.authz),
      fcl.payer(fcl.authz),
      fcl.authorizations([fcl.authz]),
      fcl.limit(35),
      //fcl.args([fcl.arg(recipient, t.Address)]),
      fcl.transaction`
        import FreshArtNFT from 0xFreshArtNFT

        transaction {

          // The reference to the collection that will be receiving the NFT
          let receiverRef: &{FreshArtNFT.NFTReceiver}
      
          // The reference to the Minter resource stored in account storage
          let minterRef: &FreshArtNFT.NFTMinter
      
          prepare(acct: AuthAccount) {
              // Get the owner's collection capability and borrow a reference
              self.receiverRef = acct.getCapability<&{FreshArtNFT.NFTReceiver}>(/public/NFTReceiver)
                  .borrow()
                  ?? panic("Could not borrow receiver reference")
              
              // Borrow a capability for the NFTMinter in storage
              self.minterRef = acct.borrow<&FreshArtNFT.NFTMinter>(from: /storage/NFTMinter)
                  ?? panic("could not borrow minter reference")
          }
      
          execute {
              // Use the minter reference to mint an NFT, which deposits
              // the NFT into the collection that is sent as a parameter.
              let newNFT <- self.minterRef.mintNFT()
      
              self.receiverRef.deposit(token: <-newNFT)
      
              log("NFT Minted and deposited to Account 2's Collection")
          }
      }    
      `,
    ])
    .then(fcl.decode)

  return fcl.tx(txId).onceSealed()
}