import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { query } from "@/lib/flow/query"

/// Returns on array of the NFTViews (without data) from an address' collection.
export async function readArtIdsInSet(setId: number): Promise<number[] | null> {

  const code = `import FreshArtNFT from 0xFreshArtNFT
    pub fun main(setId: UInt32): [UInt32]? {
      return FreshArtNFT.getArtIdsInSet(setId: setId)
    }`
  const args: any[] = [
    fcl.arg(setId, t.UInt32)
  ]
  const result: number[] | null = await query(code, args)

  return result


  /*
  // HARDCODE set 0 for now   TODO
  return await query(`import FreshArtNFT from 0xFreshArtNFT
  pub fun main(): [UInt32]? {
    return FreshArtNFT.getArtIdsInSet(setId: 1)
  }`)
  */
}