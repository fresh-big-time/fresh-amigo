import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { query } from "@/lib/flow/query"
import { NFTView } from "@/lib/flow/models"

/// Returns on array of the NFTViews (without data) from an address' collection.
export async function readCollectionNFTViews(address: string): Promise<NFTView[]> {
  const code = `import FreshArtNFT from 0xFreshArtNFT
    pub fun main(address: Address): [FreshArtNFT.NFTView] {
      let account = getAccount(address)
      let collectionRef = account.getCapability(FreshArtNFT.CollectionPublicPath).borrow<&{FreshArtNFT.FreshArtCollectionPublic}>()
          ?? panic("Could not borrow capability from public collection")
      return collectionRef.getNFTViews()
    }`

  const args: any[] = [
    fcl.arg(address, t.Address)
  ]

  const result: NFTView[] = await query(code, args)
  return result
}