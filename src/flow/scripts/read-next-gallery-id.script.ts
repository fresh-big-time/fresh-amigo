import { query } from "@/lib/flow/query"

export async function getNextGalleryId(): Promise<number> {

  const result: number = await query(`import FreshDealer from 0xFreshDealer
    pub fun main(): UInt32 {
      return FreshDealer.nextGalleryId
    }
  `)
  return result
}