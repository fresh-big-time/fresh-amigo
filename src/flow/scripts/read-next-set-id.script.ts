import * as fcl from "@onflow/fcl"
import { query } from "@/lib/flow/query"

export async function getNextSetId(): Promise<number> {

  const result: number = await query(`
    import FreshArtNFT from 0xFreshArtNFT

    pub fun main(): UInt32 {
      return FreshArtNFT.nextSetId
    }
  `)

  return result

  /*
  return fcl
    .send([
      fcl.script`
        import FreshArtNFT from 0xFreshArtNFT

        pub fun main(): UInt32 {
          return FreshArtNFT.nextSetId
        }
      `
    ])
    .then(fcl.decode)
    */
}