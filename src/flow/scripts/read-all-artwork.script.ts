import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { query } from "@/lib/flow/query"
import { ArtworkResource } from "@/lib/flow/models"

// Reads all artwork in the FreshArtNFT. Artwork, not individual NFTs.
export async function readAllArtwork(): Promise<ArtworkResource[]> {
  const code = `import FreshArtNFT from 0xFreshArtNFT
  pub fun main(): [FreshArtNFT.Art] {
    return FreshArtNFT.getAllArtwork()
  }`

  const result: ArtworkResource[] = await query(code)
  return result
}