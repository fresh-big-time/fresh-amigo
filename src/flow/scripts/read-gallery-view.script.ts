import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"
import { query } from "@/lib/flow/query"
import { DealerGalleryView } from "@/lib/flow/models"


import { productionCreds, testCreds} from "@/lib/flow/creds"

// Fetches Gallery View from owner address
// Parameters:
// dealerAddress: Address for FreshDealer owner
// galleryId: Dealer gallery id
//
// pub struct GalleryView
//    pub let galleryId: UInt32
//    pub let isOpen: Bool
//    pub let prices: [UFix64]
//    pub let collections: {UInt32: [FreshArtNFT.NFTView]}
export async function readGalleryView(galleryId: number): Promise<DealerGalleryView> {

  const code = `import FreshDealer from 0xFreshDealer
    pub fun main(dealerAddress: Address, galleryId: UInt32): FreshDealer.GalleryView {
      let dealerAccount = getAccount(dealerAddress)
      let dealer = dealerAccount.getCapability(FreshDealer.DealerPublicPath).borrow<&{FreshDealer.Public}>()
              ?? panic("Could not borrow Dealer.")
      return dealer.getGalleryViewFor(galleryId: galleryId)
  }`

  // TODO testCreds to prodCreds
  const args: any[] = [
    fcl.arg(testCreds.config["0xFreshDealer"], t.Address),
    fcl.arg(galleryId, t.UInt32)
  ]

  const result: DealerGalleryView = await query(code, args)
  return result

  /*
  // HARDCODE FOR NOW. TODO
  return await query(`import FreshDealer from 0xFreshDealer
  pub fun main(): FreshDealer.GalleryView {
    let dealerAccount = getAccount(0x1d4eb4552f566229)
    let dealer = dealerAccount.getCapability(FreshDealer.DealerPublicPath).borrow<&{FreshDealer.Public}>()
            ?? panic("Could not borrow Dealer.")
    return dealer.getGalleryViewFor(galleryId: 1)
  }`)
  */
}