import { createContext, useContext, ReactNode, useState } from "react"

/// Simple domain hash based on ga.js, outputs short number sequence.
/// gist.github.com/iperelivskiy/4110988
const hash = function(s) {
  var a = 1, c = 0, h, o;
  if (s) {
    a = 0;
    for (h = s.length - 1; h >= 0; h--) {
      o = s.charCodeAt(h);
      a = (a << 6 & 268435455) + o + (o << 14);
      c = a & 266338304;
      a = c !== 0 ? a ^ c >> 21 : a;
    }
  }
  return String(a);
}

export type AppContextType = {
  isProduction: boolean
  isTest: boolean
  isLocal: boolean
}

export function valuesForHost(host: string): AppContextType {
  const hostHash = hash(window.location.host)
  const values: AppContextType = {
    isProduction: (hostHash === process.env.NEXT_PUBLIC_PRODUCTION_HASH),
    isTest: (hostHash === process.env.NEXT_PUBLIC_TEST_HASH),
    isLocal: (hostHash === process.env.NEXT_PUBLIC_LOCAL_HASH)
  }
  if (values.isLocal) {
    const envHash = hash(process.env.NEXT_PUBLIC_LOCAL_ENVIRONMENT)
    values.isProduction = (envHash === process.env.NEXT_PUBLIC_PRODUCTION_HASH)
    values.isTest = (envHash === process.env.NEXT_PUBLIC_TEST_HASH)
  }
  return values
}

const appContextDefaultValues: AppContextType = {
  isProduction: false,
  isTest: false,
  isLocal: false
}

const AppContext = createContext<AppContextType>(appContextDefaultValues)

export function useAppContext() {
  return useContext(AppContext)
}

type Props = {
  children: ReactNode
}


export function AppProvider({ children }: Props) {
  /*
  const [ isProduction, setIsProduction ] = useState<boolean>(null)
  const [ isTest, setIsTest ] = useState<boolean>(null)
  const [ isLocal, setIsLocal ] = useState<boolean>(null)
  const [ isAdmin, setIsAdmin ] = useState<boolean>(null)

  console.log(`-- hostIs ${host}`)
  setIsProduction(host === process.env.HOST_IS_PRODUCTION)
  setIsTest(host === process.env.HOST_IS_TEST)
  setIsLocal(host === process.env.HOST_IS_LOCAL)
  setIsAdmin(process.env.MONGODB_ISADMIN === "true")
  */

  //if (value === undefined) {
  //  value = appContextDefaultValues
  //}
  var value: AppContextType = appContextDefaultValues
  if (typeof window !== 'undefined') {
    value = valuesForHost(window.location.host)
  }


  return (
    <AppContext.Provider value={value}>
      {children}
    </AppContext.Provider>
  )
}
