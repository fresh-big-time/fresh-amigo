import Link from 'next/link'

type ArtProps = {
  title: string
  edition: number
}

export default function ArtDetail({ title, edition }: ArtProps) {
  const art = {
    artist: 'Artist Name',
    slug: 'slug',
    src: '/art/art_002.svg',
    title: 'Title of the Work'
  }

  return <Link href="/artist/{art.artist}/art/{art.slug}">
    <figure className="art">
      <img src={art.src} alt={art.title} />
      <figcaption>
        <div className="detail">
          <label>Title</label>
          <div>{art.title}</div>
        </div>
        <div className="detail">
          <label>Title</label>
          <div>{art.artist}</div>
        </div>
        <div className="detail">
          <label>Editions</label>
          <div>10001</div>
        </div>
        <div className="detail">
          <label>Serial</label>
          <div>10001</div>
        </div>
      </figcaption>
    </figure>
  </Link>
}