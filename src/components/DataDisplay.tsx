import React, { PropsWithChildren, useState } from 'react'

import DataEdit from '@/components/DataEdit'

interface Props<T> {
  model: T,
  defaultData: T,
  largeTextKeys: string[],
  onSubmit: (T) => void
}

function DataDisplay<ObjectType extends { /* id: number */ }>(
  { model, defaultData, largeTextKeys, onSubmit }: PropsWithChildren<Props<ObjectType>>,
) {
  const [isEdit, setIsEdit] = useState(false)
  const isNew = (model == undefined)

  if (isNew) {
    console.log(`isNew`)
  }

  if (isNew || isEdit) {
    return (
    <DataEdit
      model={model}
      defaultData={defaultData}
      largeTextKeys={largeTextKeys}
      onSubmit={(obj) => {onSubmit(obj); setIsEdit(false);}}
      onCancel={() => setIsEdit(false)} />
    )
  }
  
  return (<div className="panel">
  <div className="grid grid-larger-tiles">
    {
    Object.keys(model).map((key, i) => {
      return(
        <div className={`data-row ${largeTextKeys.includes(key) ? 'data-large' : ''}`} key={i}>
          <label>{key}</label>
          <article>{model[key]}</article>
        </div>
        )
    })
    }
  </div>
  <div className="panel-aside">
    <button onClick={() => setIsEdit(true) }>Edit</button>
  </div>
</div>)
}

export default DataDisplay