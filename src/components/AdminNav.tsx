import Link from 'next/link'

export default function AdminNav({current}) {
  return <nav className="links-list">
  <Link href="/admin">
    <a className={current === 'sets' ? 'active' : ''}>Sets</a>
  </Link>
  <Link href="/admin/drafts">
    <a className={current === 'drafts' ? 'active' : ''}>Artwork Drafts</a>
  </Link>
  <Link href="/admin/artwork">
    <a className={current === 'artwork' ? 'active' : ''}>Artwork</a>
  </Link>
  <Link href="/admin/minting">
    <a className={current === 'minting' ? 'active' : ''}>Minting</a>
  </Link>
  <Link href="/admin/dealer">
    <a className={current === 'dealer' ? 'active' : ''}>Dealer</a>
  </Link>
  </nav>
}