import Link from 'next/link'
import styles from 'styles/Art.module.css'

export default function ArtThumbnail() {
  const art = {
    artist: 'name',
    slug: 'slug',
    src: '/art/art_003.svg',
    title: 'Title'
  }

  return <Link href={`/artist/${art.artist}/art/${art.slug}`}>
    <div className="art-frame">
      <div className="art">
        <img src={art.src} alt={art.title} />
      </div>
      <h2>{art.title}</h2>
      <b>{art.artist}</b>
      <div className="detail no-bottom-border">
        <span>$300.00</span>
        <span>x110</span>
      </div>
    </div>
  </Link>
}