import Link from 'next/link'
import AuthCluster from 'components/AuthCluster'
//import InitCluster from "components/InitCluster"
import { useCurrentUser } from "state/current-user"


export default function Nav() {
  const cu = useCurrentUser()
    
  return <nav className="nav">
    <Link href="/">
      <img src="/img/freshbigtime.svg" alt="FRESH Big Time" className="logo" />
    </Link>
  </nav>
}

/*


    <Link href="/user/user01/collection">My Collection</Link>
    <AuthCluster/>

    
    <InitCluster address={cu.addr} />

  {#if $session.user}
    <button on:click={logout}>Logout</button>
  {:else}
    <button on:click={login}>Login</button>
  {/if}
*/