import Link from 'next/link'
import { ArtworkResource, GalleryRow } from '@/lib/flow/models'

interface Props {
  row: GalleryRow,
  name: string,
  checked: boolean,
  chances: number,
  onChange: (boolean) => void
}

export default function GalleryArtRow({ row, name, checked, chances, onChange }: Props) {
  const art = row.art

  return (<div className="gallery-row">
    <label htmlFor={row.art.artId.toString()}>
      <input type="checkbox" name={name} id={row.art.artId.toString()} value={row.art.artId}
           checked={checked} onChange={onChange} />

      {!checked && (<svg className="check" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
        <rect x="4" y="4" fill="none" stroke="#c7c8c9" strokeWidth="3" strokeMiterlimit="10" width="25" height="25"/>
      </svg>)}

      {checked && (<svg className="check" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
        <rect x="4" y="4" fill="#ffcf01" stroke="#00A69C" strokeWidth="3" strokeMiterlimit="10" width="25" height="25"/>
        <rect x="3" y="3" fill="none" stroke="#e83894" strokeWidth="3" strokeMiterlimit="10" width="25" height="25"/>
        <polyline fill="none" stroke="#00A69C" strokeWidth="6" strokeMiterlimit="10" points="4,15 13,23 29,8 "/>
        <polyline fill="none" stroke="#6551a2" strokeWidth="6" strokeMiterlimit="10" points="4,13 13,21 29,6 "/>
      </svg>)}

      <div className="art" key={art.artId}>
        <object
          data={`data:image/svg+xml;base64,${btoa(art.data1 + art.data2)}`}
          type={art.mediaType}
          title={art.metadata.title}></object>
      </div>
      <h2>{art.artId}: {art.metadata.title}</h2>

    </label>

    <div>
      <div>
        <div>
          <b>{chances?.toFixed(2)}% </b>
          {row.available} Available Serials
        </div>
        <div className="serials">
          {row.serials.map((is, i) => {
            i += 1
            if (is) {
              return <div key={i}>{i}</div>
            }
            return <a key={i} href="#">{i}</a>
          })}
        </div>
      </div>
    </div>

    <Link  href="/artist/{art.artist}/art/{art.slug}"> View</Link>
  </div>)
}