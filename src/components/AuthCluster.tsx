import React, { useState, useEffect } from "react"
import { useCurrentUser } from "state/current-user"

function AuthIsLoggedIn() {
  const cu = useCurrentUser()

  // <span>{cu.addr ?? "No Address"}</span>

  return !cu.loggedIn ? null : (
    <>
      Username
      <button onClick={cu.logOut}>Log Out</button>
    </>
  )
}

function AuthIsNotLoggedIn() {
  const cu = useCurrentUser()

  return cu.loggedIn ? null : (
    <>
      <button onClick={cu.logIn}>Log In</button>
      <button onClick={cu.signUp}>Sign Up</button>
    </>
  )
}

export default function AuthCluster() {
  return (
    <>
      <AuthIsLoggedIn />
      <AuthIsNotLoggedIn />
    </>
  )
}