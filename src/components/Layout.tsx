import Nav from 'components/Nav'

export default function Layout({ children }) {

  const NEXT_PUBLIC_ENVIRONMENT = process.env.NEXT_PUBLIC_ENVIRONMENT


  return <div className="layout bg-light bg-shapes">
    <Nav/>
    <div className="nav-height"></div>
    <main className="content">
      {children}
    </main>
    <footer className="content">
      ALPHA VERSION - {NEXT_PUBLIC_ENVIRONMENT}
    </footer>
  </div>
}