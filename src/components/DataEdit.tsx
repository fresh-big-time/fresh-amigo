import React, { PropsWithChildren } from 'react'
import { useForm } from "react-hook-form"


interface Props<T> {
  model: T,
  defaultData: T,
  largeTextKeys: string[],
  onSubmit: (T) => void,
  onCancel: () => void
}

function DataDisplay<ObjectType extends { /* id: number */ }>(
  { model, defaultData, largeTextKeys, onSubmit, onCancel }: PropsWithChildren<Props<ObjectType>>,
) {
  const { register, handleSubmit, watch, formState: { errors } } = useForm()

  if (model == undefined) {
    model = defaultData
  }
  
  return (<form onSubmit={handleSubmit(onSubmit)}>
  <div className="panel">
  <div className="grid grid-larger-tiles">
    {
    Object.keys(model).map((key, i)=> {

      const input = () => {
        if (largeTextKeys.includes(key)) {
          return (<textarea name={key} id={key} defaultValue={model[key]} {...register(key)}></textarea>)
        } else if (typeof model[key] === 'boolean') {
          return (<input type='checkbox' name={key} defaultValue={model[key]} {...register(key)} />)
        }
        const type = (typeof model[key] === 'number') ? 'number' : 'text'
        return (<input type={type} name={key} id={key}  defaultValue={model[key]} {...register(key)} />)
      }

      return (
        <div className="data-row" key={i}>
          <label htmlFor={key}>{key}</label>
          { input() }
        </div>
      )
    })
    }
  </div>
  <div className="panel-aside">
    <button type="submit">Submit</button>
    { onCancel != null && (<button onClick={() => onCancel() }>Cancel</button>) }
  </div>
</div>
</form>)
}

// {!isNew && <button onClick={() => setIsEdit(false) }>Cancel</button>}

export default DataDisplay