/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    Coded by Tim, May-June, 2021
 */

import { connectToDatabase } from '@/lib/data/mongodb'
import { LedgerData } from '@/lib/data/models'

/// Record a completed sale to ledger
export async function recordSaleToLedger(nftId: number, buyerName: string, buyerAddress: string, price: number) {
  const record: LedgerData = {
    nftId: nftId,
    buyerName: buyerName,
    buyerAddress: buyerAddress,
    price: price
  }
  const { db } = await connectToDatabase()
  await db.collection('ledger').collection.insertOne(record)
}

/// Record the NFTs in a fullfilled pack to ledger
/*
export async function recordPackToLedger(pack: PackData, buyerName: string, buyerAddress: string) {  
  const records: LedgerData[] = pack.nftIds.map(function(nftId): LedgerData {
    return {
      nftId: nftId,
      buyerName: buyerName,
      buyerAddress: buyerAddress,
      packId: pack.packId
    }
  })
  const { db } = await connectToDatabase()
  await db.collection('ledger').collection.insertMany(records)
}
*/
