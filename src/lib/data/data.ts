/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    Coded by Tim, May-June, 2021
 * 
 *    Models for MongoDB Data
 */

import { ObjectId } from 'mongodb'
import { connectToDatabase } from '@/lib/data/mongodb'
import { ArtData, EditionData, SaleData, SetData, DraftMintData, DraftArtData } from '@/lib/data/models'

/**
 * Wrapper functions for MongoDB db backend.
 * But another backend service could be switched in.
 */

/// @returns List of documents
async function list(name: string, query?: {}): Promise<any> {
  const { db } = await connectToDatabase()
  return await db
      .collection(name)
      .find(query)
  //    .sort({ field: -1 })
  //    .limit(20)
      .toArray()
  
}
/// @returns True id documented updated/inserted
async function updateOrInsert(name: string, query: {}, value: any): Promise<boolean> {
  const { db } = await connectToDatabase()
  const collection = await db.collection(name)
  const res = await collection.updateOne(query, { $set: value }, { upsert: true })
  return res.upsertedId != null
}
/// @returns Number of documents deleted
async function deleteMany(name: string, query: {}): Promise<number> {
  const { db } = await connectToDatabase()
  const collection = await db.collection(name)
  const res = await collection.deleteMany(query)
  return res.deletedCount
}



export async function listSets(): Promise<SetData[]> {
  return list('sets')
}

export async function addSet(set: SetData): Promise<boolean> {
  return updateOrInsert('sets', { 'setId': set.setId }, set)
}

export async function listArt(setId: number | null): Promise<ArtData[]> {
  return list('artwork', {'setId': setId})
}

export async function addArt(art: ArtData): Promise<boolean> {
  const query = { 'artId': art.artId, 'setId': art.setId }
  return updateOrInsert('artwork', query, art)
}

export async function listEditions(setId: number, artId: number): Promise<EditionData[]> {
  return list('editions', {'setId': setId, 'artId': artId})
}

export async function addEditions(editions: EditionData[]): Promise<number> {
  const { db } = await connectToDatabase()
  const collection = await db.collection('editions')
  const res = await collection.insertMany(editions)
  return res.insertedIds.length
}

export async function listSales(setId?: number, artId?: number): Promise<SaleData[]> {
  let query = {}
  if (typeof setId !== 'undefined' && !isNaN(setId)) {
    query['setId'] = setId
  }
  if (typeof artId !== 'undefined' && !isNaN(artId)) {
    query['artId'] = artId
  }
  const sales = await list('sales', query)  
  return sales as SaleData[]
}

export function addSale(sale: SaleData): Promise<boolean> {
  return updateOrInsert('sales', { 'nftId': sale.nftId }, sale)
}

export function removeSale(nftId: number): Promise<number> {
  return deleteMany('sales', {'nftId': nftId })
}


export async function listDraftMints(): Promise<DraftMintData[]> {
  return list('draftMint')
}

export async function addDraftMint(setId: number, artId: number, quantity: number): Promise<boolean> {
  const draft: DraftMintData = {
    setId: setId,
    artId: artId,
    quantity: quantity,
    isMinted: false
  }
  const { db } = await connectToDatabase()
  const res = await db.collection('draftMint').collection.insertOne(draft)
  return (res.insertedCount > 0)
}

export async function draftMintSetIsMinted(draft: DraftMintData): Promise<boolean> {
  const { db } = await connectToDatabase()
  const id = new ObjectId(draft._id)
  const res = await db.collection('draftMint').updateOne({'_id': id}, { $set: {'isMinted': true} })
  return (res.modifiedCount > 0)
}

export async function listDraftArtwork(setId?: number): Promise<DraftArtData[]> {
  const { db } = await connectToDatabase()
  const collection = await db.collection('draftArtwork')
  // List excludes the data fields.
  const exclude = {} //{ data1: 0, data2: 0 }
  if (isNaN(setId)) {
    return await collection.find().project(exclude).toArray()
  }

  return await collection.find({'setId': setId})
                  .project(exclude)
                  .toArray()
}

export async function findOneDraftArtwork(_id: string): Promise<DraftArtData> {
  const { db } = await connectToDatabase()
  const id = new ObjectId(_id)
  return await db.collection('draftArtwork').findOne({'_id': id})
}

export async function addDraftArt(draft: DraftArtData): Promise<string> {
  const { db } = await connectToDatabase()
  var res
  if (draft._id) {
    const id = new ObjectId(draft._id)
    delete draft._id
    res = await db.collection('draftArtwork').updateOne({'_id': id}, { $set: draft}, {upsert: true})
  } else {
    res = await db.collection('draftArtwork').insertOne(draft)
  }
  return res.insertedId
}

export async function draftArtworkSetIsMinted(draft: DraftArtData): Promise<boolean> {
  const { db } = await connectToDatabase()
  const id = new ObjectId(draft._id)
  const res = await db.collection('draftArtwork').updateOne({'_id': id}, { $set: {'isMinted': true} })
  return (res.modifiedCount > 0)
}
