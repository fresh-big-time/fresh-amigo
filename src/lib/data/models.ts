/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    Coded by Tim, May-June, 2021
 */

import { ObjectId } from 'mongodb'

export type Message = {
  message: string
}

export type ArtData = {
  _id?: ObjectId,
  artId: number,
  setId?: number, // not initially added to a set on chain
  mediaType: string,
  dataType: string,
  numberMinted: number,
  retired: boolean,

  metadata_title: string,
  metadata_artist: string,
  metadata_set: string,
  metadata_description: string,
  
  data1: string,
  data2: string,
}

export type DraftArtData = {
  _id?: ObjectId,
  setId: number,
  mediaType: string,
  dataType: string,
  metadata_title: string,
  metadata_artist: string,
  metadata_set: string,
  metadata_description: string,
  
  data1: string,
  data2: string,

  isMinted: boolean
}

export type EditionData = {
  nftId: number,
  serialNumber: number,
  artId: number,
  setId: number,
  lastPrice: number,
  penultPrice: number,
  saleCount: number
  ownerName: string,
  ownerAddress: string,
  status: string  // NEW, DEALER, OWNED
}

export type SaleData = {
  setId: number,
  artId: number,
  nftId: number,
  price: number,
  royalties: number,
  ownerAddress: string
}

export type LedgerData = {
  nftId: number,
  buyerName: string,
  buyerAddress: string,
  price?: number,
  proceeds?: number
}

export type SetData = {
  setId: number,
  name: string,
  description: string,
  isLocked: boolean
}


export type DraftMintData = {
  _id?: ObjectId,
  setId: number,
  artId: number,
  quantity: number,
  isMinted: boolean
}
