
export type Credentials = {
  privateAccountAddress: string,
  privateKeyId: number,
  privateKey: string,
  config: any
}


export const productionCreds: Credentials = {
  privateAccountAddress: process.env.PROD_FLOW_ACCOUNT_ADDRESS,
  privateKeyId: Number(process.env.PROD_FLOW_ACCOUNT_KEY_ID),
  privateKey: process.env.PROD_FLOW_ACCOUNT_PRIVATE_KEY,
  config: {
    "accessNode.api": "https://access-mainnet-beta.onflow.org",
    "discovery.wallet": "https://fcl-discovery.onflow.org/mainnet/authn",
    "0xADMINACCOUNT": "",
    "0xFreshArtNFT": "",
    "0xFreshDealer": "",
    "0xProfile": "",
    "0xFUSD": "0x3c5959b568896393",
    "0xFungibleToken": "0xf233dcee88fe0abe",
    "0xNonFungibleToken": "0x1d7e57aa55817448"
  }
}

export const testCreds: Credentials = {
  privateAccountAddress: process.env.TEST_FLOW_ACCOUNT_ADDRESS,
  privateKeyId: Number(process.env.TEST_FLOW_ACCOUNT_KEY_ID),
  privateKey: process.env.TEST_FLOW_ACCOUNT_PRIVATE_KEY,
  config: {
    "accessNode.api": "https://access-testnet.onflow.org",
    "discovery.wallet": "https://fcl-discovery.onflow.org/testnet/authn",
    "0xADMINACCOUNT": "0x1d4eb4552f566229",
    "0xFreshArtNFT": "0x1d4eb4552f566229",
    "0xFreshDealer": "0x1d4eb4552f566229",
    "0xProfile": "0xba1132bc08f82fe2",
    "0xFUSD": "0xe223d8a629e49c68",
    "0xFungibleToken": "0x9a0766d93b6608b7",
    "0xNonFungibleToken": "0x631e88ae7f1d7c20"
  }
}