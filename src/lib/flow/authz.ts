import * as fcl from "@onflow/fcl"
import { SHA3 } from 'sha3'
import { ec } from 'elliptic'
const EC = new ec('p256') //EC('secp256k1')

import { productionCreds, testCreds} from "./creds"

//
// https://github.com/onflow/flow-js-sdk/blob/master/packages/fcl/src/wallet-provider-spec/authorization-function.md
//
// https://github.com/onflow/fcl-dev-wallet/blob/main/src/authz.js
//

function hash(message): Buffer {
  const sha = new SHA3(256)
  sha.update(Buffer.from(message, "hex"))
  return sha.digest()
}

function sign(privateKey: string, message: string): string {
  const key = EC.keyFromPrivate(Buffer.from(privateKey, "hex"))
  const sig = key.sign(hash(message))
  const n = 32
  const r = sig.r.toArrayLike(Buffer, "be", n)
  const s = sig.s.toArrayLike(Buffer, "be", n)
  return Buffer.concat([r, s]).toString("hex")
}

// alias Hex = String
// type signable = { message: Hex, voucher: voucher }
// type compositeSignature = { addr: String, keyId: Number, signature: Hex }
// signingFunction :: signable -> compositeSignature
// type account = { tempId: String, addr: String, keyId: Number, signingFunction: signingFunction }
// authz :: account -> account

export async function authz(account) {
  const {
    privateAccountAddress: address,
    privateKeyId: keyId,
    privateKey: key,
  } = testCreds
  // TODO production when production

  return {
    // there is stuff in the account that is passed in
    // you need to make sure its part of what is returned
    ...account,
    // the tempId here is a very special and specific case.
    // what you are usually looking for in a tempId value is a unique string for the address and keyId as a pair
    // if you have no idea what this is doing, or what it does, or are getting an error in your own
    // implementation of an authorization function it is recommended that you use a string with the address and keyId in it.
    // something like... tempId: `${address}-${keyId}`
    tempId: `${address}-${keyId}`,
    addr: fcl.sansPrefix(address), // eventually it wont matter if this address has a prefix or not, sadly :'( currently it does matter.
    keyId: keyId, // must be a number
    signingFunction: signable => ({
      addr: fcl.withPrefix(address), // must match the address that requested the signature, but with a prefix
      keyId: keyId, // must match the keyId in the account that requested the signature
      signature: sign(key, signable.message), // signable.message |> hexToBinArray |> hash |> sign |> binArrayToHex
      // if you arent in control of the transaction that is being signed we recommend constructing the 
      // message from signable.voucher using the @onflow/encode module
    }),
  }
}