import * as fcl from "@onflow/fcl"
import * as t from "@onflow/types"

import { authz } from "@/lib/flow/authz"
import { productionCreds, testCreds} from "./creds"

export type TransactionResult = {
  id: string
  error: string
  events: {data: any}[]
}

function config() {
  // TODO production when production
  fcl.config(testCreds.config)
}

/**
 * Runs a script
 * @returns Result of script in any type it resolves to, I promise
 */
export async function query(code: string, args?: any[]): Promise<any> {
  config()
  return fcl.query({ cadence: code, args: () => args })
}

/**
 * Run a transaction with the Admin authorization from api server side.
 * @param code    Cadence code for transaction
 * @param args    FCL Arguments array ie. [ fcl.arg("Joe", t.String), fcl.arg(4, t.Int), ]
 * @returns       A TransactionResult, I promise.
 */
export async function mutateWithAdmin(code: string, args?: any[]): Promise<TransactionResult> {
  return mutate(code, authz, args)
}

/**
 * Run a transaction with the FCL wallet authorization from client site.
 * @param code    Cadence code for transaction
 * @param args    FCL Arguments array ie. [ fcl.arg("Joe", t.String), fcl.arg(4, t.Int), ]
 * @returns       A TransactionResult, I promise.
 */
 export async function mutateWithWallet(code: string, args?: any[]): Promise<TransactionResult> {
  return mutate(code, fcl.authz, args)
}


export async function mutate(code: string, authz: any, args?: any[]): Promise<TransactionResult> {
  console.log("SENDING TRANSACTION")
  
  config()
  const response = await fcl.send([
    fcl.transaction(code),
    fcl.args(args),
    fcl.proposer(authz),
    fcl.authorizations([authz]),
    fcl.payer(authz),
    fcl.limit(9999),
  ])

  const { transactionId } = response
  const { error, events } = await fcl.tx(response).onceSealed()

  console.log(`TX id: ${transactionId}`)
  console.log(`TX error: ${error}`)

  return {
    id: transactionId,
    error: error,
    events: events,
  }
}

