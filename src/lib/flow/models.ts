/*
 *    _______   _______     _______        _  ,   ____  ____
 *     | | \|    | | \ '.    | | \|     .|` '\|    | |  | |
 *     | |  '    | |  | |    | |  '    | |    |    | |  | |
 *     | |  ,    | |  | |    | |  ,    |  \        | |  | |
 *     | |-<|    | |-< <     | |-<|     \   '.     | |--| |
 *     | |  '    | |  | |    | |  '       '.  |    | |  | |
 *     | |       | |  | |,   | |  ,    |\   | /    | |  | |
 *    _|_|_     _|_|_  \_/  _|_|_/|    |'\_.|/    _|_|__|_|_
 *
 *    Coded by Tim, Nov, 2021
 * 
 *    Models for Flow Blockchain Data
 */


// Views

export type NFTView = {
  nftId: number,
  serialNumber: number,
  artId: number,
  lastPrice: number,
  penultPrice: number,
  saleCount: number
}

export type DealerGalleryView = {
  galleryId: number,
  lowPrice: string,  // UFix64 decodes to string
  highPrice: string, // UFix64 decodes to string
  isOpen: boolean,
  prices: Array<number>,
  serials: any //Record<number, number[]>
}

// Derived from view and artwork
export type GalleryRow = {
  art: ArtworkResource,
  serials: boolean[],
  available: number
}

// Artwork

export type ArtworkResource = {
  artId: number,
  mediaType: string,
  dataType: string,
  data1: string,
  data2: string,
  royalties: Royalties,
  metadata: Metadata  
}

export type Metadata = {
  title: string,
  artist: string,
  set: string,
  description: string
}

export type Royalties = {
  artist: RoyaltiesCut,
  minter: RoyaltiesCut
}

export type RoyaltiesCut = {
  cut: string
}
