
const fcl = require("@onflow/fcl")

fcl.config().put("accessNode.api", "http://localhost:8080")

fcl.query({ cadence: `
  import FreshArtNFT from 0xf8d6e0586b0a20c7    
  pub fun main(): UInt32 {
    return FreshArtNFT.nextSetId
  }
`}).then(id => console.log(`nextSetId: ${id}`))

/*
fcl.config().put("accessNode.api", "https://access-testnet.onflow.org")

const test1: number = await fcl.query({ cadence: `import Rawr from 0xba1132bc08f82fe2
pub fun main(): Int {
  return Rawr.add(a: 40, b: 2)
}`})
console.log(`test1: ${test1}`)

const test2: number = await fcl.query({ cadence: `import Rawr from 0xba1132bc08f82fe2
pub fun main(): Int {
  return Rawr.add(a: 34, b: 53)
}`})
console.log(`test2: ${test2}`)
*/