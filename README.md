

## Google Cloud Run

PROD
https://fresh-amigo-acqaynrubq-uc.a.run.app

TEST


# CREDS

```
flow keys generate
Generating key pair with signature algorithm:                 ECDSA_P256
...
🔐 Private key (⚠️  store safely and don't share with anyone): de115fa80b221eeed38da3fee5ac0eb4d8c6b6ca22a94dee90509e3e5e6f1f56
🕊️️  Encoded public key (share freely):                         6cc4fff63a7853cd0e889996050c7e4c9e596c1ed2507d28bb497ed8ad1ce64f8906b2b41f34fe20f113793f6d216f1c3788f8e23f5f4cdfbcb5cfb51b5f0b6d
```

### Emulator

⚙️   Flow client initialized with service account:

👤  Address: 0xf8d6e0586b0a20c7
ℹ️   Start the emulator with this service account by running: flow emulator start


### Flow Testnet Faucet

```
0x1d4eb4552f566229
```

```sh
export FLOW_ADDRESS=0x1d4eb4552f566229
export FLOW_PRIVATE_KEY=de115fa80b221eeed38da3fee5ac0eb4d8c6b6ca22a94dee90509e3e5e6f1f56
```



flow.json when `flow init` ran:
```
{
	"host": "",
	"accounts": {
		"service": {
			"address": "f8d6e0586b0a20c7",
			"privateKey": "65004981cd6ab36d490adb1dda64efe2681ca4e19beb5840613812f1cd1f9ff8",
			"sigAlgorithm": "ECDSA_P256",
			"hashAlgorithm": "SHA3_256",
			"keyType": "hex",
			"keyIndex": 0,
			"keyContext": {
				"privateKey": "65004981cd6ab36d490adb1dda64efe2681ca4e19beb5840613812f1cd1f9ff8"
			}
		}
	}
}
```



# FLOW

### 3. Create a Flow Testnet account

You'll need a Testnet account to work on this project. Here's how to make one:

#### Generate a key pair 

Generate a new key pair with the Flow CLI:

```sh
flow keys generate
```

_⚠️ Make sure to save these keys in a safe place, you'll need them later._

#### Create your account

Go to the [Flow Testnet Faucet](https://testnet-faucet-v2.onflow.org/) to create a new account. Use the **public key** from the previous step.

#### Save your keys

After your account has been created, save the address and private key to the following environment variables:

```sh
# Replace these values with your own!
export FLOW_ADDRESS=0xabcdef12345689
export FLOW_PRIVATE_KEY=xxxxxxxxxxxx
```

### 4. Deploy the contracts

```sh
flow project deploy --network=testnet
```



# NEXT

https://github.com/vercel/next.js/tree/canary/examples/with-docker





This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.js`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.


## Monitor localhost tcp

```
sudo tcpdump -D
```
```
sudo tcpdump -i lo0
```

# FCL DOC

fcl.script is a builder, it is passed into fcl.send it accepts a string (or can be used as a tagged template literal)
To use arguments you also need to include types.

```
// example A
const SCRIPT = `
  pub fun main(a: Int): Int {
    return a
  }
`

await fcl.send([
  fcl.script(SCRIPT),
  fcl.args([
    fcl.arg(7, t.Int),
  ])
]).then(fcl.decode)
```
```
// or
// Example B
await fcl.send([
  fcl.script`
    pub fun main(a: Int): Int {
      return a
    }
  `,
  fcl.args([
    fcl.arg(7, t.Int)
  ])
]).then(fcl.decode)
```

fcl.query is a convenience function, it is newer, and lays the groundwork for some upcoming things we want to make available to people.
The idea is it will have common defaults for the majority of cases (with the ability to customize).
It's syntax is also closer to how people usually write javascript as well, so we are hoping it feels more familiar to people.
It accepts an object of options and automatically decodes the response.
Arguments is a function that passes in both fcl.arg and t as a convenience.

```
// Example C
await fcl.query({
  cadence: `
    pub fun main(a: Int): Int {
      return a
    }
  `,
  args: (arg, t) => [
    arg(7, t.Int),
  ]
})
```

Under the hood Example C is pretty much taking options passed into fcl.query and passing them into something that looks fairly close to Example B



The following doesn't exist yet.
There will soon be a similar convenience function for transactions called fcl.mutate that will look and work very similarly to fcl.query
```
await fcl.send([
  fcl.transaction`
    transaction(a: Int) {
      prepare(acct: AuthAccount) { /* ... * / }
    }
  `,
  fcl.limit(50),
  fcl.args([
    fcl.arg(7, t.Int)
  ]),
  fcl.proposer(fcl.authz),
  fcl.payer(fcl.authz),
  fcl.authorizations([
    fcl.authz,
  ]),
]).then(fcl.decode)
```

Will soon be able to be written like this:
```
await fcl.mutate({
  cadence: `
    transaction(a: Int) {
      prepare(acct: AuthAccount) { /* ... * / }
    }
  `,
  args: (arg, t) => [
    arg(7, t.Int),
  ],
  limit: 50
})
```


## TESTNET

if you go to any account: https://flow-view-source.com/testnet/account/0xf117a8efa34ffd58
then click authenticate in the top left, after authentication your address will show up in the bottom left, click your address.
In the sidebar of your account will be a + new contract button which will allow you to create a new contract. Save button in the bottom right, errors in the console.
Once its saved it should show up in the sidebar. you can then edit it there.
Standard rules for what you can and cant upgrade in a contract and expect things to still work apply.

also, in the console for flow-view-source, fcl and t have been attached to the window object, so you can write scripts and transaction in the console there to test them out